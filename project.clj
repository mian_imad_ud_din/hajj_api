(defproject clojure-game-geek "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.walmartlabs/lacinia-pedestal "0.5.0"]
                 [io.aviso/logging "0.2.0"]
                 [environ "1.0.0"]]
  :min-lein-version "2.0.0"
  :plugins [[environ/environ.lein "0.3.1"]]
  :resource-paths ["resources"]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "clojure-game-geek-standalone.jar"
  :profiles {:production {:env {:production true}}}
  :main ^:skip-aot clojure-game-geek.main
  )
