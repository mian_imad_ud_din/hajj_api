(ns clojure-game-geek.schema
  "Contains custom resolvers and a function to provide the full schema."
  (:require
    [clojure-game-geek.data :refer [data]]
   [clojure.java.io :as io]
   [com.walmartlabs.lacinia.util :as util]
   [com.walmartlabs.lacinia.schema :as schema]
   [clojure.edn :as edn]))

(defn resolve-game-by-id
  [games-map context args value]
  (let [{:keys [id]} args]
    (get games-map id)))

(defn resolver-map
  []
  (let [games-map (->> data
                       :games
                       (reduce #(assoc %1 (:id %2) %2) {}))]
    {:query/haj-by-id (partial resolve-game-by-id games-map)}))

(def schema '{:objects
              {:BoardGame
               {:description "A physical or virtual board game."
                :fields
                             {
                              :id                 {:type (non-null ID)}
                              :age                {:type Int}
                              :visa_issue_plac    {:type String}
                              :nationality        {:type String}
                              :airline_name       {:type String}
                              :first_name         {:type String}
                              :entry_date         {:type String}
                              :season             {:type String}
                              :passport_issue_pla {:type String}
                              :exit_time          {:type String}
                              :entry_time         {:type String}
                              :gender             {:type String}
                              :exit_date          {:type String}
                              }}}

              :queries
              {:haj_by_id
               {:type        :BoardGame
                :description "Access a BoardGame by its unique id, if it exists."
                :args
                             {:id {:type ID}}
                :resolve     :query/haj-by-id}}})

(defn load-schema
  []
  (-> schema
      (util/attach-resolvers (resolver-map))
      schema/compile))


(comment

  (io/resource "cgg-data.edn")
  )
