(ns clojure-game-geek.main
  (:require
   [clojure-game-geek.schema :as s]
   [com.walmartlabs.lacinia :as lacinia]
   [com.walmartlabs.lacinia.pedestal :as lp]
   [io.pedestal.http :as http]
   [clojure.java.browse :refer [browse-url]]
   [clojure.walk :as walk]
   [environ.core :refer [env]]))

(defn schema []  (s/load-schema))

(defn simplify
  "Converts all ordered maps nested within the map into standard hash maps, and
   sequences into vectors, which makes for easier constants in the tests, and eliminates ordering problems."
  [m]
  (walk/postwalk
   (fn [node]
     (cond
       (instance? clojure.lang.IPersistentMap node)
       (into {} node)

       (seq? node)
       (vec node)

       :else
       node))
   m))

(defn q
  [query-string]
  (-> (lacinia/execute (schema) query-string nil nil)
      simplify))

(defonce server nil)

(defn start-server
  [port _]
  (let [server (-> (schema)
                   (lp/service-map (cond-> {:graphiql true}
                                     port (assoc :port port)))
                   http/create-server
                   http/start)]
    server))

(defn stop-server
  [server]
  (http/stop server)
  nil)

(defn start
  [port]
  (alter-var-root #'server (partial start-server port))
  :started)

(defn stop
  []
  (alter-var-root #'server stop-server)
  :stopped)

(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 5000))]
    (start port)))


(comment

  (start)
  (stop)

  )
