# clojure-haj-api
A Hajj Data gateway that helps almost all domains including pilgrims data, scheduling and financial data, housing information, medical information etc. This API is aimed to provide a path to all developers to easily access and push Hajj Data from all devices. 
# Usage
For Developers: To push and get Hajj data through this API for further development
For Government of KSA : To get Hajj Data in realtime for visualization and other purposes during hajj and historic data for previous hajj
For Advertisers : (On permission of Govt. of KSA) get past data to calculate supply and demand of various products before Hajj. (Note that this service might not be free)
 
# Data Query and Sources
Clojure gets data from already converted EDN file, which is similar to JSON but has more features for better search. Clojure supports big data environment and this is ideal for the quick data push and pull from repositories. Currently, EDN files are being explicitly made from given CSV database but in future, data will be converted automatically from hajj databases to EDN files. A vast variety of databases like mysql, nosql, mongoDB etc can be integrated.
Data queried is returned in JSON format, which is most commonly used format and supports multiple languages like python etc. 
Data is queried in Clojure format, which again can be accessible 
API Installation

API is currently uploaded on Heroku (link) on public servers. You can access api via web interface and execute commands online via our REPL interface.

API commands reference
One Module of API search was implemented during this hackathon based on the dataset provided by hackathon management. 

# Module Descriptions

## haj_by_id
### Description: 
Searches for the Haji data by ID. Returns required results
### Syntax : 
_{haj_by_id(id:[id number]){required data schema}}_
### Available Schema:
:id {:type (non-null ID)}
:age {:type Int}
:visa_issue_plac {:type String}
:nationality {:type String} 
:airline_name {:type String } 
:first_name {:type String}
:entry_date {:type String}
:season {:type String}
:passport_issue_pla {:type String} <br/>
:exit_time {:type String} <br/>
:entry_time {:type String} <br/>
:gender {:type String} <br/>
:exit_date {:type String } <br/>
### Example:
_Input_

{
  haj_by_id(id: 2) {
    id
    first_name
    entry_date
    exit_date
    entry_time
    gender
    season
    airline_name
    exit_time
    visa_issue_plac
  }
}

_Output_

{
  "data": {
    "haj_by_id": {
      "id": "2",
      "first_name": "RUSHDIYAH",
      "entry_date": "14371101",
      "exit_date": "14371218",
      "entry_time": "03:54:10",
      "gender": "F",
      "season": "1437",
      "airline_name": "QR\n",
      "exit_time": "16:58:25",
      "visa_issue_plac": "132"
    }
  }
}

## haj_by_name
### Description: 
Searches for the Haji data by ID. Returns required results
### Syntax : 
{haj_by_name(name:[name of haji]){required data from available schema}}
### Available Schema:
:id {:type (non-null ID)}
:age {:type Int}
:visa_issue_plac {:type String}
:nationality {:type String}
:airline_name {:type String }
:first_name {:type String}
:entry_date {:type String}
:season {:type String}
:passport_issue_pla {:type String}
:exit_time {:type String}
:entry_time {:type String}
:gender {:type String}
:exit_date {:type String }

### Example:
coming soon... 

## haj_passport_issue
### Description: 
Searches for the Haji data by passport issue place. Returns required results
### Syntax : 
{haj_passport_issue(passport_issue_pla:[name of place]){required data from available schema}}
### Available Schema:
:id {:type (non-null ID)}
:age {:type Int}
:visa_issue_plac {:type String}
:nationality {:type String}
:airline_name {:type String }
:first_name {:type String}
:entry_date {:type String}
:season {:type String}
:passport_issue_pla {:type String}
:exit_time {:type String}
:entry_time {:type String}
:gender {:type String}
:exit_date {:type String } 
### Example:
Coming soon...

## haj_query_all
### Description: 
Returns all Hajj  by passport issue place. Returns required results
### Syntax : 
{haj_query_all(){required data from available schema}}
### Available Schema:
:id {:type (non-null ID)}
:age {:type Int}
:visa_issue_plac {:type String}
:nationality {:type String}
:airline_name {:type String }
:first_name {:type String}
:entry_date {:type String}
:season {:type String}
:passport_issue_pla {:type String}
:exit_time {:type String}
:entry_time {:type String}
:gender {:type String}
:exit_date {:type String } 

# License
_Copyright � 2018 TheHAJJApp@HajjHackathon_
Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.


